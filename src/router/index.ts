import { createRouter, createWebHistory, createMemoryHistory } from 'vue-router'

const baseUrl = import.meta.env.BASE_URL;

const router = createRouter({
  history: createMemoryHistory(baseUrl),
  routes: []
})

export default router
