import { createApp } from 'vue'
import './assets/styles/style.css'
import App from './App.vue'
import { createPinia } from 'pinia'
import "/node_modules/flag-icons/css/flag-icons.min.css"

const pinia = createPinia()

createApp(App).use(pinia).mount('#app')
