import axios from 'axios';
import { debounce } from "../helpers";
import { SearchResult } from '../stores/search';

const API_URL = 'https://nominatim.openstreetmap.org';
const config = {
    baseURL: API_URL
}

const axiosInstance = axios.create(config);
axiosInstance.interceptors.response.use((response) => {
    console.log(response)
    return response.data ?? response.status;
})

export const API = {
    async do(method = 'GET', url = '', data = {}): Promise<any> {
        const options = {
          method,
          url,
          data
        }
        return await axiosInstance(options)
            .catch(err => {
              console.warn(err)
            })
    },

    async getSearch(search: string): Promise<SearchResult[]> {
        const url = `/search?q=${search}&addressdetails=1&format=json`
        return await this.do('GET', url);
    }
}