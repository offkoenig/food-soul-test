import { defineStore } from 'pinia'
import { API } from '../api'

export const useSearchStore = defineStore('search', {
  state: () => {
    return {
        loading: false,
        results: [] as SearchResult[]
    }
  },
  actions: {
    async search(searchQuery: string) {
        this.loading = true;
        const searchResult: SearchResult[] = await API.getSearch(searchQuery)
            .finally(() => {
                this.loading = false
            })
        
        this.results = searchResult
    },
  },
})

export interface SearchResultAddress {
    country_code: string;
    country: string;
    county: string;
    city: string;
}

export interface SearchResult {
    place_id: number;
    display_name: string;
    icon: string;
    address: SearchResultAddress;
    boundingbox: string[];
    lat: string;
    lon: string;
}